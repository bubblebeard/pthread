/* 5.5. Винни-Пух и пчелы. Заданное количество пчел добывают мед равными порциями,
 *  задерживаясь в пути на случайное время. Винни-Пух потребляет мед порциями 
 * заданной величины за заданное время и столько же времени может прожить
 *  без питания. Работа каждой пчелы реализуется в порожденном процессе.*/

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <signal.h>

#define PORTION 5
#define HONEY 100
#define LOOP 1
#define MAXNTHREADS 100

struct {
    pthread_mutex_t mutex;
    int honey;
} shared = {
    PTHREAD_MUTEX_INITIALIZER, HONEY
};
void *bee_worker(void *);
int winnie(int, int);

int main(int argc, char** argv) {
    if (argc < 3) {
        printf("usage: threads <bees> <time_interval> <meal_size>");
        return (EXIT_FAILURE);
    }
    int bees = atoi(argv[1]); //кол-во пчёл
    if (bees > MAXNTHREADS) {
        printf("too many bees. terminating");
        return (EXIT_FAILURE);
    }
    int time = atoi(argv[2]); //интервал через который винни ест
    int meal = atoi(argv[3]); //сколько винни съедает за раз
    int count[bees];

    pthread_t tid_bees[bees]/*, tid_winnie*/;
    for (int i = 0; i < bees; i++) {
        count[i] = i;
        pthread_create(&tid_bees[i], NULL, bee_worker, &count[i]);
    }
    winnie(time, meal);
    for (int i = 0; i < bees; i++) {
        pthread_cancel(tid_bees[i]);
    }
    return (EXIT_SUCCESS);
}

void *bee_worker(void *arg) {
    int *id = (int*) arg;
    while (LOOP) {
        pthread_mutex_lock(&shared.mutex);
        printf("bee%d: locked\n", *id);
        fflush(stdout);
        shared.honey += PORTION;
        printf("bee%d: %d honey left\n", *id, shared.honey);
        fflush(stdout);
        pthread_mutex_unlock(&shared.mutex);
        printf("bee%d: unlocked\n", *id);
        fflush(stdout);
        sleep(rand() % 5);
    }
    return;
}

int winnie(int time, int meal) {
    while (LOOP) {
        pthread_mutex_lock(&shared.mutex);
        printf("Winnie: locked\n");
        if (shared.honey < meal) {
            printf("Winnie: not enough honey\n");
            fflush(stdout);
            pthread_mutex_unlock(&shared.mutex);
            printf("Winnie: unlocked\n");
            fflush(stdout);
            sleep(time);
            pthread_mutex_lock(&shared.mutex);
            printf("Winnie: locked\n");
            if (shared.honey < meal) {
                printf("Winnie died. Rest In Pepperonis\n");
                fflush(stdout);
                pthread_mutex_unlock(&shared.mutex);
                printf("Winnie: unlocked\n");
                return (EXIT_SUCCESS);
            } else {
                shared.honey -= meal;
                printf("Winnie eat %d honey. %d honey left\n", meal, shared.honey);
                pthread_mutex_unlock(&shared.mutex);
                printf("Winnie: unlocked\n");
                fflush(stdout);
            }
        } else {
            shared.honey -= meal;
            printf("Winnie eat %d honey. %d honey left\n", meal, shared.honey);
            pthread_mutex_unlock(&shared.mutex);
            printf("Winnie: unlocked\n");
            fflush(stdout);
            sleep(time);
        }
    }
    return (EXIT_SUCCESS);
}

